#!/bin/bash
horodate=$(date +%d-%m-%Y_%H_%M)
if [ -d $autoreload ] && [ ! -z "$autoreload" ];
then
   while inotifywait -e modify,delete,create -r $autoreload --exclude \.swp
   do
    # wait all modifications	   
     sleep 10 
     horodate=$(date +%d-%m-%Y_%H_%M)
     squid -k reconfigure
     echo "RELOAD SQUID $horodate: new files" >> /var/log/squid/reloadsquid.log
     sleep 60
     done
fi
