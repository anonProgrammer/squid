#!/bin/bash

if [ -e "/squid/squid.pid" ];then
   echo "Squid pid: Already running ?"
   rm /squid/squid.pid 
   pkill squid
   exit 1
fi

chown -R squid /var/log/squid 

# Cache exist ?
if [ ! -d "/var/spool/squid/$HOSTNAME" ];then
  squid -z -n $HOSTNAME
  sleep 5
else
  touch "/var/spool/squid/$HOSTNAME"
fi

while test -e /tmp/$HOSTNAME.pid 
do
  echo "cache is generated/checked"	
  sleep 1
done  

# Run sup: IN BACKGROUND
/squid/sup.sh &
case $? in
     1) exit 1;;
     2) exit 1;;
     *) echo -e "Sup ok or no sup";;
esac
shift

if [ $(grep -c "\-s\ /certs" /etc/squid/squid.conf) -ne 0 ];then
   if [ ! -d /certs/ssl_db ]; then 
      /usr/lib/squid/security_file_certgen -c -s /certs/ssl_db -M 600MB
   fi
   chown -R squid:squid /certs
fi

echo "Starting: squid"
# Run autoreload: IN BACKGROUND
/squid/autoreload.sh &

