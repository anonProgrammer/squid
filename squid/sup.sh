#!/bin/bash
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
if [ -z "$supgethosts" ] && [ -z "$supicaphost" ]; then
   exit 0
fi

if [ -n "$supicaphost" ]; then
   c-icap-client -i $supicaphost -p $supicapport -s request > /tmp/test-icap-e2g-orion 2>&1
   grep "200 OK" /tmp/test-icap-e2g-orion >/dev/null
   if [ $? = 0 ];then
      echo "SUP ICAP: ACCESS ICAP ok $supicaphost"
      error=0
    else
      echo "SUP ICAP: ERROR $supicaphost"
      exit 1
   fi
fi

while true
do
date=$(date +%d-%m-%Y_%w)
if [ -n "$supgethosts" ]; then
   for i in ${supgethosts[@]}
   do
     echo "CHECK: $i"
     curl -s -o /dev/null --connect-timeout 5 -A "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:88.0) Gecko/20100101 Firefox/96.0" $i
     if [ $? = 0 ];then
       echo "SUP: ACCESS ok $i"
       error=0
       break
    else
       echo "SUP: ERROR $i"
       error=1
    fi
    done
fi

# Sup error because no OK before  
if [ "$error" = 1 ];then
   echo "SUP ERROR"
   squid -k shutdown
   exit 1
fi

sleep 120
done
